# Default command for `make`:
.DEFAULT_GOAL := build

# Paths:
src.client = './client/spa'
src.server = './server/api'

# Arguments:
app         ?= client
dir         ?= ${src.client}
cmd.serve   ?= ng s -o
cmd.venv    ?= nvm ls
cmd.install ?= npm install

ifeq ($(app), server)
	dir         := ${src.server}
	cmd.serve   := ./manage.py runserver
	cmd.venv    := pipenv shell
	cmd.install := pipenv install
endif

.PHONY: serve
serve:
	@cd ${dir} && ${cmd.serve}

.PHONY: venv
venv:
	@cd ${dir} && ${cmd.venv}

.PHONY: install
install:
	@cd ${dir} && ${cmd.install}
