import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class HttpService {

  baseUrl = 'http://localhost:8000';
  httpHeaders = new HttpHeaders({
    'Content-Type': 'application/json',
  });

  constructor(private http: HttpClient) { }

  sayHi() {
    console.log("Hello World!");
  }

  getPosts() {
    return this.http.get(
      `${this.baseUrl}/posts/`,
      { headers: this.httpHeaders }
    );
  }

  getPost(id: number) {
    return this.http.get(
      `${this.baseUrl}/posts/${id}/`,
      { headers: this.httpHeaders }
    );
  }
}
