import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})

export class PostsComponent implements OnInit {
  posts: Object;

  constructor(private _http: HttpService) { }

  ngOnInit(): void {
    this
      ._http
      .getPosts()
      .subscribe(data => {
        this.posts = data;
        console.log(this.posts);
      });
  }
}
