import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { HttpService } from '../../http.service';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})

export class PostDetailComponent implements OnInit {
  post: Object = {
    title: '',
    content: '',
  };
  postId: number;

  constructor(private _route: ActivatedRoute,
              private _http: HttpService) { }

    ngOnInit(): void {
      this
        ._route
        .params
        .subscribe(data => {
          this.postId = data.id;
          console.log(this.postId);
        });
      this
        ._http
        .getPost(this.postId)
        .subscribe(data => {
          this.post = data;
          console.log(this.post);
        });
    }
}
